★ [[doc/FAQs.org][← FAQs overview page]] ★

* How to add tool X?

** The Tool-Support Page

If you can't find a specific tool on the [[../Tool-Support.org][tool support page]], you can *add a simple table entry* to it:

- If you are not familiar with git:
  1. Use the download button on the [[../Tool-Support.org][tool support page]].
  2. Modify the downloaded file and add your tool to the corresponding table.
  3. Email me the modified file to the email address which is near the bottom [[https://karl-voit.at/about/][of this page]].

- If you are familiar with git and GitLab:
  1. Use the "Edit" button on the [[../Tool-Support.org][tool support page]] OR:
  2. Create a classic pull-request:
     1. Fork or clone [[https://gitlab.com/publicvoit/orgdown][the repository]].
     2. Modify the [[../Tool-Support.org][tool support page]] and add your tool to the corresponding table.
     3. Commit your change to your own respository.
     4. Hand in a pull-request with that change.

** A Tool-Specific Page

You probably have noticed that some tools like [[../tools/Emacs.org][Emacs]] do have a
tool-specific page which contains more details on the tool itself as
well as the OD1 compatibility table.

If you do want to contribute such a tool-specific page, you can use
[[../tools/Template.org][the template file]].

- If you are not familiar with git:
  1. Use the download button on the [[../tools/Template.org][the template file]].
  2. Modify the downloaded file and add your tool according to the
     other examples.
  3. Email me the modified file to the email address which is near the bottom [[https://karl-voit.at/about/][of this page]].

- If you are familiar with git and GitLab:
  1. Fork or clone [[https://gitlab.com/publicvoit/orgdown][the repository]].
  2. Copy the =Template.org= file of the =tools= sub-folder. Choose a
     short file name that corresponds to the tool name.
  3. Modify this file and add your tool according to the
     other examples.
  4. Add a link to the tool-specific file on the [[../Tool-Support.org][tool support page]].
  5. Commit your change to your own respository.
  6. Hand in a pull-request with your changes.

---------------

★ [[doc/FAQs.org][← FAQs overview page]] ★
