★ [[../Tool-Support.org][← Back to Tool Support]] ★ [[../Overview.org][↑ Overview page]] ★

* Orgdown support for FIXXME

- Platform: /most probably one of the headings from [[../Tool-Support.org][the tool support page]]/
- License: 
- Price: 
- /link to Homepage/
- /optional link to Documentation/
- /optional link to Issue Management/
- last Orgdown1 syntax assessment: /EXAMPLE: 2021-08-12 using version X by Y/

| *Percentage of Orgdown1 syntax support:* | 0 |
#+TBLFM: @1$2=(remote(toolsupport,@>$2)/86)*100;%.0f

** Details

- legend:
  - *1* → works as simple text
  - *2* → at least one of:
    - syntax highlighting
    - support for inserting/collapse/expand/…
    - support for toggling (e.g., checkboxes, states, …)

#+NAME: toolsupport
| Element                                                                     | OD1 | OD1 comment |
|-----------------------------------------------------------------------------+-----+-------------|
| syntax elements do not need to be separated via empty lines                 |     |             |
|-----------------------------------------------------------------------------+-----+-------------|
| *bold*                                                                      |     |             |
| /italic/                                                                    |     |             |
| _underline_                                                                 |     |             |
| +strike through+                                                            |     |             |
| =code=                                                                      |     |             |
| ~commands~                                                                  |     |             |
| Combinations like *bold and /italic/ text*                                  |     |             |
| : example                                                                   |     |             |
| [[https://example.com][link *bold* test]]                                                            |     |             |
| [[https://example.com][link /italic/ test]]                                                          |     |             |
| [[https://example.com][link +strike through+ test]]                                                  |     |             |
| [[https://example.com][link =code= test]]                                                            |     |             |
| [[https://example.com][link ~commands~ test]]                                                        |     |             |
| ≥5 dashes = horizontal bar                                                  |     |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Headings using asterisks                                                    |     |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Unordered list item with "-"                                                |     |             |
| Ordered list item with 1., 2., …                                            |     |             |
| Checkbox + unordered list item with "-"                                     |     |             |
| Checkbox + ordered list item with 1., 2., …                                 |     |             |
| Mixed lists of ordered and unordered items                                  |     |             |
| Multi-line list items with collapse/expand                                  |     |             |
| Multi-line list items with support for (auto-)indentation                   |     |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Example block                                                               |     |             |
| Quote block                                                                 |     |             |
| Verse block                                                                 |     |             |
| Src block                                                                   |     |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Comment lines with "# <foobar>"                                             |     |             |
| Comment block                                                               |     |             |
|-----------------------------------------------------------------------------+-----+-------------|
| https://gitlab.com/publicvoit/orgdown plain URL without brackets            |     |             |
| [[https://gitlab.com/publicvoit/orgdown]] URL with brackets without description |     |             |
| [[https://gitlab.com/publicvoit/orgdown][OD homepage]] URL with brackets with description                              |     |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Basic table functionality                                                   |     |             |
| Table cells with *bold*                                                     |     |             |
| Table cells with /italic/                                                   |     |             |
| Table cells with _underline_                                                |     |             |
| Table cells with +strike through+                                           |     |             |
| Table cells with =code=                                                     |     |             |
| Table cells with ~commands~                                                 |     |             |
| Auto left-aligning of text                                                  |     |             |
| Auto right-aligning of numbers like "42.23"                                 |     |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Other syntax elements are interpreted at least as normal text (or better)   |     |             |
| Other syntax elements: linebreaks respected (or better)                     |     |             |
|-----------------------------------------------------------------------------+-----+-------------|
| *Sum*                                                                       |   0 |             |
#+TBLFM: @>$2=vsum(@I$2..@>>$2)

